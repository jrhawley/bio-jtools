//! Statistics for a FASTQ file.

use crate::{
    cli::CliOpt,
    record::{
        error::RecordError,
        header::{RecordName, ILLUMINA_SEPARATOR_ASCII_CODE, RNAME_SEPARATOR_ASCII_CODE},
        stats::RecordStats,
    },
    utils::{formats::OutputFormat, Fastx, Hts, HtsFile},
};
use clap::Parser;
use needletail::parse_fastx_file;
use needletail::{errors::ParseError, parser::SequenceRecord};
use std::collections::{HashMap, HashSet};
use std::path::PathBuf;

/// CLI options for getting info from an HTS file
#[derive(Debug, Parser)]
pub(crate) struct FastqInfoOpts {
    /// Get info about this HTS file
    #[clap(name = "HTS")]
    hts_path: PathBuf,

    /// Count the total number of records
    #[clap(short, long)]
    total: bool,

    /// Track the frequency of sequence lengths
    #[clap(short, long)]
    lengths: bool,

    /// Track the sequencing instruments used (only valid for FASTQ, SAM, BAM, and CRAM files)
    #[clap(short = 'I', long)]
    instruments: bool,

    /// Track the run IDs
    #[clap(short = 'R', long)]
    runs: bool,

    /// Track flow cell IDs
    #[clap(short = 'F', long)]
    flow_cell_ids: bool,

    /// Track flow cell lanes
    #[clap(short = 'L', long)]
    lanes: bool,

    /// Output format to return statistics in
    #[clap(short = 'f', long, default_value = "human")]
    format: OutputFormat,

    /// Keep statistics on the first N records
    #[clap(short = 'N', long = "max-records", value_name = "N")]
    n_max_records: Option<u64>,
}

impl FastqInfoOpts {
    /// Get information and statistics about a desired FASTQ file
    fn calc_fastq_info(&self, hts: HtsFile) -> FastqStats {
        let mut stats = FastqStats::new();
        let mut reader = parse_fastx_file(hts.path()).expect("Error opening HTS file");

        if let Some(n_max) = self.n_max_records {
            // check if the max capacity has been hit
            while let (true, Some(record)) = (stats.n_records() < n_max, reader.next()) {
                stats.process_record(&record, self);
            }
        } else {
            while let Some(record) = reader.next() {
                stats.process_record(&record, self);
            }
        }

        stats
    }
}

impl CliOpt for FastqInfoOpts {
    fn exec(&self) -> anyhow::Result<()> {
        let hts = HtsFile::new(&self.hts_path);
        match hts.filetype() {
            Hts::Fastx(Fastx::Fastq) => {
                let stats = self.calc_fastq_info(hts);
                stats.print_human_readable();
            }
            _ => todo!(),
        }

        Ok(())
    }
}

/// Important information about a sequencing configuration to record.
#[derive(Clone, Debug, Default, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub(crate) struct SeqConfigKey {
    /// The sequencing instrument ID.
    pub(crate) instrument: Option<String>,

    /// The sequencing run ID.
    pub(crate) run: Option<String>,

    /// The flow cell ID.
    pub(crate) flow_cell: Option<String>,

    /// The flow cell lane number.
    pub(crate) lane: Option<String>,
}

impl SeqConfigKey {
    /// Create a new key.
    pub(crate) fn new(
        instrument: Option<String>,
        run: Option<String>,
        flow_cell: Option<String>,
        lane: Option<String>,
    ) -> Self {
        Self {
            instrument,
            run,
            flow_cell,
            lane,
        }
    }

    /// A shorthand for creating an all-`None` key.
    pub(crate) fn none() -> Self {
        Self::new(None, None, None, None)
    }

    /// Check if all the configuration options are not being checked (i.e. just the total count).
    pub(crate) fn is_none(&self) -> bool {
        self.instrument.is_none()
            && self.run.is_none()
            && self.flow_cell.is_none()
            && self.lane.is_none()
    }

    /// Move up to the next highest level of the sequencing configuration hierarchy.
    pub(crate) fn move_up(&self) -> Self {
        if self.lane.is_some() {
            Self {
                instrument: self.instrument.clone(),
                run: self.run.clone(),
                flow_cell: self.flow_cell.clone(),
                lane: None,
            }
        } else if self.flow_cell.is_some() {
            Self {
                instrument: self.instrument.clone(),
                run: self.run.clone(),
                flow_cell: None,
                lane: None,
            }
        } else if self.run.is_some() {
            Self {
                instrument: self.instrument.clone(),
                run: None,
                flow_cell: None,
                lane: None,
            }
        } else {
            Self::none()
        }
    }
}

/// Statistics from a FASTQ file
#[derive(Debug)]
pub(crate) struct FastqStats {
    /// Total number of valid records
    valid_records: u64,

    /// Total number of invalid records
    invalid_records: u64,

    /// Total number of bases in a file
    bases: u64,

    /// Length distribution of records
    lengths: HashMap<u64, u64>,

    /// Configuration of sequencing instruments, runs, flow cells, and lanes
    config: HashMap<SeqConfigKey, u64>,
}

impl FastqStats {
    /// Process an Illumina (Casava >= v1.8) formatted FASTQ record
    fn process_illumina_split_record(&mut self, rname: &[u8], opts: &FastqInfoOpts) {
        // Illumina Casava >= v1.8 format
        let mut id_splits = rname.split(|x| *x == ILLUMINA_SEPARATOR_ASCII_CODE);
        let mut key = SeqConfigKey::default();

        // construct the relevant HashMap key from the desired options
        // need to do the allocations now, because the buffer references will be de-allocated on the next `id_splits.next()` call.
        let instrument_bytes = id_splits.next();
        if opts.instruments {
            if let Some(bytes) = instrument_bytes {
                if let Ok(id) = String::from_utf8(bytes.to_vec()) {
                    key.instrument = Some(id);
                }
            }
        };

        let run_bytes = id_splits.next();
        if opts.runs {
            if let Some(bytes) = run_bytes {
                if let Ok(id) = String::from_utf8(bytes.to_vec()) {
                    key.run = Some(id);
                }
            }
        };

        let flow_cell_bytes = id_splits.next();
        if opts.flow_cell_ids {
            if let Some(bytes) = flow_cell_bytes {
                if let Ok(id) = String::from_utf8(bytes.to_vec()) {
                    key.flow_cell = Some(id);
                }
            }
        };

        let lane_bytes = id_splits.next();
        if opts.lanes {
            if let Some(bytes) = lane_bytes {
                if let Ok(id) = String::from_utf8(bytes.to_vec()) {
                    key.lane = Some(id);
                }
            }
        };

        if !key.is_none() {
            self.track_illumina_config(key);
        }
    }

    /// Process an Illumina (Casava >= v1.8) formatted FASTQ record accoring to what the user wants to record.
    fn track_illumina_config(&mut self, key: SeqConfigKey) {
        if let Some(val) = self.config.get_mut(&key) {
            *val += 1;
        } else {
            self.config.insert(key, 1);
        }
    }
}

impl<'a> RecordStats<'a> for FastqStats {
    type Record = SequenceRecord<'a>;
    type Error = ParseError;
    type InfoOpts = FastqInfoOpts;

    /// Create a new set of statistics for a FASTQ file
    fn new() -> Self {
        FastqStats {
            valid_records: 0,
            invalid_records: 0,
            bases: 0,
            lengths: HashMap::new(),
            config: HashMap::new(),
        }
    }

    fn n_valid(&self) -> u64 {
        self.valid_records
    }

    fn n_invalid(&self) -> u64 {
        self.invalid_records
    }

    fn mut_lengths(&mut self) -> &mut HashMap<u64, u64> {
        &mut self.lengths
    }

    fn process_valid_record(&mut self, seq: &SequenceRecord, opts: &FastqInfoOpts) {
        self.valid_records += 1;

        let seq_length: u64 = seq.num_bases().try_into().unwrap();
        self.bases += seq_length;

        if opts.lengths {
            self.update_lengths(seq_length);
        }

        if opts.instruments || opts.runs || opts.flow_cell_ids || opts.lanes {
            match RecordName::try_from(seq.id()) {
                Ok(RecordName::CasavaV1_8) => {
                    let mut splits = seq.id().split(|x| *x == RNAME_SEPARATOR_ASCII_CODE);
                    let a = splits.next().unwrap();
                    self.process_illumina_split_record(a, opts);
                }
                Ok(RecordName::SequenceReadArchive) => {
                    self.process_sra_split_record();
                }
                Err(RecordError::UncertainRecordNameFormat) => todo!(),
            }
        }
    }

    fn process_invalid_record(&mut self) {
        self.invalid_records += 1;
    }

    fn print_human_readable(&self) {
        // sort the keys so that we can sum them
        let mut keys: Vec<SeqConfigKey> = self.config.keys().cloned().collect();
        keys.sort();

        let instruments: HashSet<Option<String>> =
            keys.iter().map(|key| key.instrument.clone()).collect();
        let runs: HashSet<Option<String>> = keys.iter().map(|key| key.run.clone()).collect();
        let flow_cells: HashSet<Option<String>> =
            keys.iter().map(|key| key.flow_cell.clone()).collect();
        let lanes: HashSet<Option<String>> = keys.iter().map(|key| key.lane.clone()).collect();

        // store the sums across all sequencing config hierarchies
        let mut sums: HashMap<SeqConfigKey, u64> = HashMap::new();

        // sum up all the values as necessary
        for key in keys {
            // this should never fail, because we're using its keys in the loop
            let count = self.config.get(&key).unwrap();

            // record the total for this config key
            if let Some(summed_count) = sums.get_mut(&key) {
                *summed_count += count;
            } else {
                sums.insert(key.clone(), *count);
            }

            // only deal with the higher up keys if we're not already at the top
            if !key.is_none() {
                let mut higher_key = key.move_up();
                loop {
                    // record the total for the next higher-up level in the hierarchy
                    if let Some(summed_count) = sums.get_mut(&higher_key) {
                        *summed_count += count;
                    } else {
                        sums.insert(higher_key.clone(), *count);
                    }

                    // once we've reached the top, there's nowhere else to go
                    if higher_key.is_none() {
                        break;
                    } else {
                        higher_key = higher_key.move_up()
                    }
                }
            }
        }

        println!(
            "Records: {} (Valid: {}, Invalid: {})",
            self.n_valid() + self.n_invalid(),
            self.n_valid(),
            self.n_invalid()
        );
        for inst_id in &instruments {
            let total_inst_key = SeqConfigKey::new(inst_id.clone(), None, None, None);
            if let Some(count) = sums.get(&total_inst_key) {
                println!(
                    "├─Instrument {}: {:>10}",
                    inst_id.clone().unwrap_or("".to_string()),
                    count
                );
            }

            for run_id in &runs {
                let total_run_key = SeqConfigKey::new(inst_id.clone(), run_id.clone(), None, None);
                if let Some(count) = sums.get(&total_run_key) {
                    println!(
                        "│  ├─Run {}: {:>10}",
                        run_id.clone().unwrap_or("".to_string()),
                        count
                    );
                }

                for flow_cell_id in &flow_cells {
                    let total_flow_cell_key = SeqConfigKey::new(
                        inst_id.clone(),
                        run_id.clone(),
                        flow_cell_id.clone(),
                        None,
                    );
                    if let Some(count) = sums.get(&total_flow_cell_key) {
                        println!(
                            "│  │  ├─Flow Cell {}: {:>10}",
                            flow_cell_id.clone().unwrap_or("".to_string()),
                            count
                        );
                    }

                    for lane_id in &lanes {
                        let total_lane_key = SeqConfigKey::new(
                            inst_id.clone(),
                            run_id.clone(),
                            flow_cell_id.clone(),
                            lane_id.clone(),
                        );
                        if let Some(count) = sums.get(&total_lane_key) {
                            println!(
                                "│  │  │  ├─Lane {}: {:>10}",
                                lane_id.clone().unwrap_or("".to_string()),
                                count
                            );
                        }
                    }
                }
            }
        }
        // wrap everything up cleanly
        println!("└──┴──┴──┘");
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let expected = 4;
        let observed = 2 + 2;

        assert_eq!(expected, observed);
    }
}
