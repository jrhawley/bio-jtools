//! # Processing SAM, BAM, and CRAM alignment files
//! Functions and methods related to processing alignment files, such as [SAM, BAM](https://samtools.github.io/hts-specs/SAMv1.pdf), and [CRAM](https://samtools.github.io/hts-specs/CRAMv3.pdf) files.

pub mod filter;
pub mod info_stats;
pub mod reader;
